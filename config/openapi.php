<?php

use OpenApi\Annotations as OA;

require __DIR__.'/../vendor/autoload.php';

$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->loadEnv(__DIR__.'/../.env');

define('OPENAPI_HOST', getenv('OPENAPI_HOST'));

/**
 * @OA\Server(
 *     url=OPENAPI_HOST
 * )
 */
