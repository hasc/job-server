# abc-job-server

[![pipeline status](https://gitlab.com/hasc/job-server/badges/master/pipeline.svg)](https://gitlab.com/hasc/job-server/commits/master)

A Symfony application providing the job server from [AbcJobServerBundle](https://github.com/aboutcoders/job-server-bundle).

**Note: This project is still experimental!**

## Demo

You can find a demo [here](https://gitlab.com/hasc/abc-job-demo/).

## Environment Variables

### `APP_ENV`

This variable is optional, it specifies the application runtime environment. Supported values are `prod` and `dev` (default is `prod`).

### `APP_DEBUG`

This variable is optional, it specifies whether the application is executed in debug mode (default is false).

### `APP_SECRET`

This variable is mandatory, it specifies the Symfony application secret (see: <https://symfony.com/doc/current/reference/configuration/framework.html#secret>)

### `DATABASE_URL`

This variable is mandatory, it specifies the URL of the database (see: <http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url>)

### `ENQUEUE_DSN`

This variable is mandatory, it specifies the DSN of the enqueue transport, see: <https://github.com/php-enqueue/enqueue-dev/blob/master/docs/bundle/config_reference.md>

### `TRUSTED_PROXIES`

This variable is optional, it is part of the Symfony framework configuration. Please see: <https://symfony.com/doc/current/reference/configuration/framework.html#trusted-proxies>

### `TRUSTED_HOSTS`

This variable is optional, it is part of the Symfony framework configuration. Please see: <https://symfony.com/doc/current/reference/configuration/framework.html#trusted-hostsl>

## Building the application

```bash
docker build -t job-server:local .
```

## Commands

### Process Job Replies

```bash
bin/console abc:process:reply --help
```

### Schedule Jobs

```bash
bin/console abc:schedule --help
```

## OpenApi

### Generate the OpenApi documentation

Use the following command if the API is served under localhost port 80

```bash
./vendor/bin/openapi -o public/api-doc.json vendor/abc/job vendor/abc/api-problem
```
Use the following command if the API is served under a different URL

```bash
OPENAPI_HOST="http://domain.tld:8000" ./vendor/bin/openapi -o public/api-doc.json vendor/abc/job vendor/abc/api-problem config --bootstrap config/openapi.php
```

Note: The environment variable `OPENAPI_HOST` must be set or defined in the `.env` file

Serve the OpenApi documentation

```bash
 docker run -p 80:8080 -e SWAGGER_JSON=/app/api-doc.json -v /path/to/job-server/public:/app swaggerapi/swagger-ui
```

Dockerfiles are created based on <https://sagikazarmark.hu/blog/containerizing-a-symfony-application/>

## License

The MIT License (MIT). Please see [License File](./LICENSE) for more information.
