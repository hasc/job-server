<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191116221231 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE abc_job_route (name VARCHAR(25) NOT NULL, queue VARCHAR(25) NOT NULL, reply_to VARCHAR(20) NOT NULL, PRIMARY KEY(name)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE abc_cron_job (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', schedule VARCHAR(250) NOT NULL, scheduledTime INT DEFAULT NULL, name VARCHAR(20) DEFAULT NULL, job_json LONGTEXT NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE abc_job (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', type VARCHAR(25) NOT NULL, position SMALLINT DEFAULT NULL, name VARCHAR(20) DEFAULT NULL, status VARCHAR(20) NOT NULL, input LONGTEXT DEFAULT NULL, output LONGTEXT DEFAULT NULL, allow_failure TINYINT(1) NOT NULL, processing_time DOUBLE PRECISION NOT NULL, external_id VARCHAR(36) DEFAULT NULL, completed_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_BCF41DFD727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE abc_job ADD CONSTRAINT FK_BCF41DFD727ACA70 FOREIGN KEY (parent_id) REFERENCES abc_job (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE abc_job DROP FOREIGN KEY FK_BCF41DFD727ACA70');
        $this->addSql('DROP TABLE abc_job_route');
        $this->addSql('DROP TABLE abc_cron_job');
        $this->addSql('DROP TABLE abc_job');
    }
}
