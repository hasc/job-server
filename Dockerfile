# first stage: FPM base image
FROM php:7.3.15-fpm-alpine as base

WORKDIR /app

# Install dependencies
RUN set -xe \
    && apk add --no-cache bash icu-dev \
    && docker-php-ext-install pdo pdo_mysql intl pcntl

CMD ["php-fpm"]

# second stage: Composer
FROM composer:1.9.3 as composer

RUN rm -rf /app && mkdir /app
WORKDIR /app

COPY composer.* /app/

ARG APP_ENV=prod

RUN set -xe \
    && if [ "$APP_ENV" = "prod" ]; then export ARGS="--no-dev"; fi \
    && composer install --prefer-dist --no-scripts --no-progress --no-suggest --no-interaction $ARGS

COPY . /app

RUN composer dump-autoload --classmap-authoritative
RUN composer run-script link-assets
RUN bin/console assets:install

# third stage: application image
FROM base

ARG APP_ENV=prod
ARG APP_DEBUG=0

ENV APP_ENV $APP_ENV
ENV APP_DEBUG $APP_DEBUG

COPY --from=composer /app/ /app/

# Memory limit increase is required by the dev image
RUN php -d memory_limit=256M bin/console cache:clear
